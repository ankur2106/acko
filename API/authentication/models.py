# coding: utf-8
from sqlalchemy import BigInteger, Boolean, Column, DateTime, ForeignKey, Integer, Numeric, SmallInteger, String, Text, UniqueConstraint, text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()
metadata = Base.metadata


class AuthUser(Base):
	__tablename__ = 'auth_user'
	id =Column(Integer,primary_key=True)
	password = Column(String)
	last_login = Column(DateTime)
	first_name =Column(String)
	last_name = Column(String)
	