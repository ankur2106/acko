from rest_framework import status
from rest_framework.views import APIView
from django.contrib.auth.hashers import make_password, check_password
from django.views import View
from rest_framework.response import Response
from django.shortcuts import render,render_to_response
from authentication.models import *
from authentication.serializers import *
from sqlalchemy.orm import *
from sqlalchemy import Column, String, Integer, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import *
from sqlalchemy import and_
from django.http import HttpResponse,HttpResponseRedirect
from sqlalchemy.orm import sessionmaker
import sqlalchemy, sqlalchemy.orm
import urllib
import collections
from rest_framework.decorators import api_view
from django.http import JsonResponse
from datetime import datetime
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators import csrf
from django.contrib import auth
from django.utils.decorators import method_decorator
from django.core.validators import validate_email
from django.contrib import auth
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout
from vokalApi import settings
from django.contrib.auth.decorators import login_required
from models import *
from ipware.ip import get_ip
from django.contrib.auth import get_user_model
from django.contrib.auth import logout
from django.utils import timezone
import base64
import time
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view,authentication_classes,permission_classes


# database connection function
def postgresconnection(db):
	dialect = 'postgresql'
	driver = 'psycopg2'
	username = 'postgres'
	password = 'secret007'
	server = 'localhost'
	port = '5432'
	database = db
	url=dialect+'+'+driver+'://'+username+':'+password+'@'+server+':'+port+'/'+database
	try:
		engine = create_engine(url)
		Session = sqlalchemy.orm.sessionmaker(bind=engine)
		session = Session()
		return session
	except:
		return ("Could not establish connection")


@api_view(['GET', 'POST'])
# @permission_classes((AllowAny,))
def Logout(request):
	session = postgresconnection('ankur_db')
	current_user_id = request.user.id
	logout_time = timezone.now()
	updated_values = {AuthUser.last_login :logout_time}
	session.query(AuthUser).filter(AuthUser.id == current_user_id).update(updated_values)
	session.commit()
	logout(request)
	return Response(0)
