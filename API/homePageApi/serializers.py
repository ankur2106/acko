from homePageApi.models import *
import serpy


class ViewSerializer(serpy.Serializer):
	page=serpy.Field()
	pageOrder=serpy.Field()
	pageUrl=serpy.Field()
	
class InfoBoxScenarioSerialize(serpy.Serializer):
	scenario_name=serpy.Field()
	
class UserViewApiSerializer(serpy.Serializer):
	customer_id=serpy.Field()	
	page_name=serpy.Field()
	page_order=serpy.Field()	
	widget_type=serpy.Field()
	widget_name=serpy.Field()
	widget_order=serpy.Field()
	api_id=serpy.Field()
	api_url=serpy.Field()

class UserPreferenceSerializer(serpy.Serializer):
	preference_name = serpy.Field()
	preference_value = serpy.Field()
	
class ModuleSerializer(serpy.Serializer):
	module_name = serpy.Field()
	module_link = serpy.Field()
	icon = serpy.Field()
	module_id = serpy.Field()

class ModulePageSerializer(serpy.Serializer):
	module_name = serpy.Field()	
	page_name = serpy.Field()

class custListSerializer(serpy.Serializer):
	id=serpy.Field()
	customer_name=serpy.Field()
	
class AssetDetailSerializer(serpy.Serializer):
	asset_address_book_number=serpy.Field()
	ssid=serpy.Field()
	asset_type=serpy.Field()
	make=serpy.Field()
	model=serpy.Field()
	asset_name=serpy.Field()
	asset_id=serpy.Field()
	asset_image=serpy.Field()
	customer_asset_no=serpy.Field()
	imo=serpy.Field()
	hull=serpy.Field()

class AssetGeofenceList(serpy.Serializer):
	fence_name=serpy.Field()
	

		
class AssetInfoSerializer(serpy.Serializer):
	asset_name=serpy.Field()
	ss_ID=serpy.Field()
	# status=serpy.Field()
	# status_code=serpy.Field()
	# connectivity=serpy.Field()
	asset_address_book_number=serpy.Field()
	watchList=serpy.Field()

class WatchListSerializer(serpy.Serializer):
	user_id=serpy.Field()
	last_name=serpy.Field()
	asset_id=serpy.Field()
	asset_name=serpy.Field()
	

class FilterListSerializer(serpy.Serializer):	
	value=serpy.Field()
	label=serpy.Field()
	

class AssetStatSerializer(serpy.Serializer):	
	watchlist_asset_count=serpy.Field()	
	all_asset_count=serpy.Field()
	
class apiListSerializer(serpy.Serializer):
	api_url=serpy.Field()

class cascadeFilterSerializer(serpy.Serializer):
	id=serpy.Field()



class WidgetListSerializer(serpy.Serializer):
	page_id=serpy.Field()
	widget_id=serpy.Field()
	widget_name=serpy.Field()
	page_name=serpy.Field()
	

class TotalAssetSerializer(serpy.Serializer):
	total_assets=serpy.Field()
	active=serpy.Field()
	idle=serpy.Field()	
	off=serpy.Field()
	connection_lost=serpy.Field()
	
class WatchListSerializer(serpy.Serializer):
	watch_list_count=serpy.Field()

class GeoFenceSerializer(serpy.Serializer):
	inside_geoFence_count=serpy.Field()
	outside_geoFence_count=serpy.Field()
	
class ListFilterSerializer(serpy.Serializer):
	name=serpy.Field()
	api=serpy.Field()
	status=serpy.Field()
	
class AssetFilterListSerializer(serpy.Serializer):
	value=serpy.Field()
	asset_address_book_number=serpy.Field()
	ss_ID=serpy.Field()
	asset_type=serpy.Field()
	make=serpy.Field()
	model=serpy.Field()
	# status=serpy.Field()
	# connectivity=serpy.Field()
	# geofence_status=serpy.Field()
	# alarms=serpy.Field()
	asset_name=serpy.Field()
	asset_id=serpy.Field()
	# status_code=serpy.Field()
	# map_icon=serpy.Field()
	watchlist_flag=serpy.Field()
	label=serpy.Field()
	asset_image=serpy.Field()
	customer_asset_no=serpy.Field()
	
class GeoFenceListSerializer(serpy.Serializer):
	path=serpy.Field()
	fence_id=serpy.Field()
	asset_id=serpy.Field()
	fence_name=serpy.Field()	

class AssetListSerializer(serpy.Serializer):	
	value=serpy.Field()
	label=serpy.Field()
	ssId=serpy.Field()
	name=serpy.Field()
		
class UserAssetListSerializer(serpy.Serializer):	
	ssId=serpy.Field()

	'''82 serialisers'''
class CustomersSerializer(serpy.Serializer):
	id=serpy.Field()
	customer_name=serpy.Field()
	
class FencesSerializer(serpy.Serializer):
	id=serpy.Field()
	fence_name=serpy.Field()
	fence_boundary=serpy.Field()
	active_flag=serpy.Field()
	asset_cnt=serpy.Field()

class getDigitalSubsystemSerializer(serpy.Serializer):
	scenario_name=serpy.Field()
	subSystem=serpy.Field()
	subSystem_order=serpy.Field()

	
class getSubsystemsSerializer(serpy.Serializer):
	scenario_name=serpy.Field()
	ss_id=serpy.Field()
	operating_unit_name=serpy.Field()
	model=serpy.Field()
	machine_unit_name=serpy.Field()
	parent_operating_unit_id=serpy.Field()
	child_operating_unit_id=serpy.Field()
	level=serpy.Field()
	operating_unit_measurement_name=serpy.Field()
	operating_unit_measurement_type=serpy.Field()
	operating_unit_measurement_tag_id=serpy.Field()
	lcl=serpy.Field()
	ucl=serpy.Field()
	unit_name=serpy.Field()
	parameterOrder=serpy.Field()
	multiplicand=serpy.Field()
	denominator=serpy.Field()
	from_offset=serpy.Field()
	to_offset=serpy.Field()
	parameterIconType=serpy.Field()
	parameterIconPath=serpy.Field()
	map_value_flag=serpy.Field()
	ouocId=serpy.Field()
	
class getSubsystemParametersSerializer(serpy.Serializer):
	level=serpy.Field()
	parent_name=serpy.Field()
	child_name=serpy.Field()
	ss_id=serpy.Field()
	operating_unit_measurement_name=serpy.Field()
	operating_unit_measurement_lcl=serpy.Field()
	operating_unit_measurement_ucl=serpy.Field()
	unit_name=serpy.Field()
	
class getParameterDetailSerializer(serpy.Serializer):
	assetName=serpy.Field()
	ssName=serpy.Field()
	ss_id=serpy.Field()
	manufacturer_model_code=serpy.Field()
	manufacturer_model_name=serpy.Field()
	operating_unit_measurement_name=serpy.Field()
	lcl=serpy.Field()
	ucl=serpy.Field()
	unit_name=serpy.Field()
	multiplicand=serpy.Field()
	denominator=serpy.Field()
	from_offset=serpy.Field()
	to_offset=serpy.Field()
	
class valMapListSerializer(serpy.Serializer):
	ouocValue=serpy.Field()
	ouocMapValue=serpy.Field()
	type=serpy.Field()

class getNotificationTypeSerializer(serpy.Serializer):
	notifType=serpy.Field()
	notifName=serpy.Field()

class parameterAlertsSerializer(serpy.Serializer):
	parameter=serpy.Field()
	operating_unit_id=serpy.Field()
	rule_id=serpy.Field()
	event_timestamp_epoch=serpy.Field()
	event_occurence_flag=serpy.Field()
	rule_name=serpy.Field()
	
class getUnitSystemsSerializer(serpy.Serializer):
	id=serpy.Field()
	unit_system_name=serpy.Field()

class scenarioAlertCountSerializer(serpy.Serializer):
	sum=serpy.Field()
	operating_unit_id=serpy.Field()
	parameter=serpy.Field()
	
class getParameterRelationsSerializer(serpy.Serializer):
	asset_id=serpy.Field()	
	operating_unit_measurement_tag_id=serpy.Field()
	operating_characteristic_type_name=serpy.Field()	
	related_operating_unit_operating_characteristic_name=serpy.Field()
	related_operating_unit_operating_characteristic_tag_id=serpy.Field()
	unit=serpy.Field()
	
class resultSerializer(serpy.Serializer):
	operating_unit_serial_number=serpy.Field()	
	operating_unit_name=serpy.Field()
	