from django.conf.urls import url
import views

urlpatterns = (
	url(r'^fetchTimelineData/$', views.fetchTimelineData,name='fetchTimelineData'),
)