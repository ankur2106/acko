from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import render
from homePageApi.models import *
from sqlalchemy.orm import *
from sqlalchemy import Column, String, Integer, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import *
from sqlalchemy.orm import sessionmaker
import sqlalchemy, sqlalchemy.orm
from sqlalchemy.dialects.postgresql import array
from django.http import JsonResponse
from datetime import datetime
import collections
from sqlalchemy.pool import NullPool
from sqlalchemy.sql.functions import coalesce
from datetime import datetime, timedelta
from datetime import *
from rom import util
import requests
from django.db import connection
# below are required if need to check any api without auth
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view,authentication_classes,permission_classes
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
import copy

import pandas
import datetime
import numpy
import scipy.spatial

def date_func(ts):
    return pandas.to_datetime(ts).date()

def getDistance(x):
    l = list(x)
    formerSequence = numpy.array(l[:(len(l)/2)])
    latterSequence = numpy.array(l[(len(l)/2):])
    
    formerSequence = formerSequence.reshape((1, formerSequence.shape[0]))
    latterSequence = latterSequence.reshape((1, latterSequence.shape[0]))
    return scipy.spatial.distance.cdist(formerSequence, latterSequence, 'euclidean')

def anomalyAnalysis(data,parameter):
	data['DateTime'] = pandas.to_datetime(data['timestamp'],format='%d-%m-%Y %H:%M') 
	data = data[['DateTime',parameter,'Maintainence','Accidents']]

	data['Distance'] = data[parameter].rolling(window = 144,center=True).apply(lambda x:getDistance(x))
	data['Distance'] = (data['Distance'] - data['Distance'].min())/(data['Distance'].max() - data['Distance'].min())

	data['SuggestedMaintainance'] = 0
	data.loc[data['Distance'] > 0.5,'SuggestedMaintainance'] = 1

	data['Date'] = data['DateTime'].apply(date_func)
	
	meanTempDaywise = pandas.DataFrame(data.groupby('Date')[parameter].apply(lambda x:x.mean()))
	meanTempDaywise['Date'] = meanTempDaywise.index
	maintainanceDaywise = pandas.DataFrame(data.groupby('Date')['Maintainence'].apply(lambda x:x.sum()))
	maintainanceDaywise['Date'] = maintainanceDaywise.index
	accidentDaywise = pandas.DataFrame(data.groupby('Date')['Accidents'].apply(lambda x:x.sum()))
	accidentDaywise['Date'] = accidentDaywise.index
	suggestedMaintainanceDaywise = pandas.DataFrame(data.groupby('Date')['SuggestedMaintainance'].apply(lambda x:x.sum()))
	suggestedMaintainanceDaywise['Date'] = suggestedMaintainanceDaywise.index

	datewise = meanTempDaywise.merge(maintainanceDaywise,on='Date')
	datewise = datewise.merge(accidentDaywise,on='Date')
	datewise = datewise.merge(suggestedMaintainanceDaywise,on='Date')
		
	datewise.loc[(datewise['SuggestedMaintainance'] >= 1),'SuggestedMaintainance'] = 1
	datewise['Maintainance'] = datewise['Maintainence'].rolling(window = 3,center=True).apply(lambda x:x.max())
	datewise['Status'] = 0
	datewise.loc[(datewise['SuggestedMaintainance'] == 1) & (datewise['Maintainance'] == 0),'Status'] = 1
	
	statusCount = datewise['Status'].max()
	
	eventData = datewise.loc[datewise['SuggestedMaintainance']==1,:]

	eventData['LagEventDate'] = eventData['Date'].transform('shift')
	
	try:
		eventData['intermediateCount'] = (eventData['Date'] - eventData['LagEventDate']).dt.days
	except:
		eventData['intermediateCount'] = numpy.NaN

	eventData['LagEventDate'] = eventData['LagEventDate'].fillna('NA')
	eventData.loc[eventData['LagEventDate'] == 'NA','Count'] = 0
	eventData.loc[eventData['intermediateCount'] > 1,'Count'] = 0

	eventData.loc[eventData['Count'] == 0,'Event Start Date'] = eventData['Date']
	eventData['Event Start Date'] = eventData['Event Start Date'].fillna(method='ffill')
	eventData['Count'] = eventData.groupby(['Event Start Date']).cumcount()

	eventData['MaxCount'] = eventData.groupby(['Event Start Date'])['Count'].transform(lambda x:max(x))
	eventData.loc[eventData['Count'] == eventData['MaxCount'],'Event End Date'] = eventData['Date']
	eventData['Event End Date'] = eventData['Event End Date'].fillna(method='bfill')
	eventData['Count_Days'] = (eventData['Event End Date'] - eventData['Event Start Date']).dt.days + 1
	eventData = eventData.loc[eventData['Count_Days']>0]

	events = pandas.DataFrame(eventData.groupby(['Event Start Date','Event End Date'])['Maintainence'].apply(lambda x:x.max()))
	events['ID'] = events.index
	events['Event Start Date'] = events.apply(lambda x:x['ID'][0] ,axis=1)
	events['Event End Date'] = events.apply(lambda x:x['ID'][1] ,axis=1)

	mdata = datewise.loc[datewise['Maintainence']==1,:]
	mdata['Date'] = pandas.to_datetime(mdata['Date'])
	mdata['Date'] = mdata['Date'].apply(lambda x: ((x - datetime.datetime(1970,1,1)).total_seconds())*1000)

	adata = datewise.loc[datewise['Accidents']==1,:]
	adata['Date'] = pandas.to_datetime(adata['Date'])
	adata['Date'] = adata['Date'].apply(lambda x: ((x - datetime.datetime(1970,1,1)).total_seconds())*1000)
	
	return events,list(mdata['Date']),list(adata['Date']),statusCount


def summaryAnalysis(basePath):
	dataList = [
			{'system' : 'Engine','Parameter' : 'Temperature','PCount':2,'Index':0},
			{'system' : 'Engine','Parameter' : 'Speed','PCount':2,'Index':0},
			{'system' : 'Brake','Parameter' : 'FrictionTemperature','PCount':2,'Index':1},
			{'system' : 'Brake','Parameter' : 'ABS','PCount':2,'Index':1}
			]

	compiledData = None
	mList = []
	aList = []
	statusCount = 0 
	for item in dataList:
		data = pandas.read_csv(basePath+item['system']+'.csv')
		events,mdata,adata,count = anomalyAnalysis(data,item['Parameter'])
		events['System'] = item['system']
		events['Parameter'] = item['Parameter']
		events['TotalParameterCount'] = item['PCount']
		events['Index'] = int(item['Index'])
		mList =  mList + mdata
		aList =  aList + adata
		statusCount = statusCount + count
		if compiledData is None:
			compiledData = events
		else:
			compiledData = compiledData.append(events)
			
	summary = compiledData.groupby(['System','Index','Event Start Date','Event End Date'])['TotalParameterCount'].agg(['count','max'])
	summary['PercentageIssue'] = (summary['count'] / summary['max'] )

	summary = summary[['PercentageIssue']]
	
	summary['ID'] = summary.index
	summary['x'] = summary.apply(lambda x:x['ID'][2] ,axis=1)
	summary['x2'] = summary.apply(lambda x:x['ID'][3] ,axis=1)
	summary['y'] = summary.apply(lambda x:x['ID'][1] ,axis=1)
	summary['partialFill'] = summary['PercentageIssue']
	
	summary.to_csv(r'D:\\ACKO\\Output.csv')
	
	summary['x2'] = summary['x2'] + datetime.timedelta(days = 1)
	
	summary['x'] = summary['x'].apply(lambda x: ((x - datetime.datetime(1970,1,1)).total_seconds())*1000)
	summary['x2'] = summary['x2'].apply(lambda x: ((x - datetime.datetime(1970,1,1)).total_seconds())*1000)

	summary = summary[['x','x2','y','partialFill']].to_dict('records')
	mList = list(set(mList))
	aList = list(set(aList))
	
	formatDict = {
	   'dashStyle': 'longdashdot',
	   'width': 2,
	   'label': {
		 'align': 'left',
	   }
	 }
	 
	lineData = []

	for e in mList:
		data = copy.deepcopy(formatDict)
		data['color'] = 'orange'
		data['value'] = e
		data['label']['text'] = 'Maintainance'
		lineData.append(data)
		
	for e in aList:
		data = copy.deepcopy(formatDict)
		data['color'] = 'red'
		data['value'] = e
		data['label']['text'] = 'Accident'
		lineData.append(data)

	return summary,lineData,statusCount

    
@csrf_exempt
def fetchTimelineData(request):
	finalOutput ={}
	timeLine,lineList,status = summaryAnalysis(r'D:\\ACKO\\')
	finalOutput['lines'] = lineList
	finalOutput['timeLine'] = timeLine
	finalOutput['count'] = {'count':str(status)}
	print finalOutput
	return JsonResponse(finalOutput)