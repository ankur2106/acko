app.controller('vokalWebController',function($http,$scope,$compile,$timeout,NgMap,apiResults) {
	var vm = this;
	$scope.data = {};
	url='fetchTimelineData/';
	param = {};
	apiResults(url,param).success(function(data){
		console.log("data>>>>",data);
		$scope.data = data;		
		$scope.plotGraph();
	});
	
	$scope.data1 = {
		'alarmDates':[],
		'faults':[{	
				x: Date.UTC(2014, 10, 21),
				x2: Date.UTC(2014, 10, 25),
				y: 0,
				partialFill: 0.15
			}, {
				x: Date.UTC(2014, 11, 2),
				x2: Date.UTC(2014, 11, 5),
				y: 1,
				partialFill: 0.1
			}, {
				x: Date.UTC(2014, 11, 8),
				x2: Date.UTC(2014, 11, 9),
				y: 2,
				partialFill: 0.15
			}, {
				x: Date.UTC(2014, 11, 9),
				x2: Date.UTC(2014, 11, 11),
				y: 1,
				partialFill: 0.2
			}, {
				x: Date.UTC(2014, 11, 10),
				x2: Date.UTC(2014, 11, 23),
				y: 2,
				partialFill: 0.95
			}, {
				x: Date.UTC(2014, 11, 21),
				x2: Date.UTC(2014, 11, 23),
				y: 1,
				partialFill: 1.0
			}, {
				x: Date.UTC(2014, 11, 21),
				x2: Date.UTC(2014, 11, 23),
				y: 0,
				partialFill: 0.9
			}]
	}

	$scope.plotGraph =function() {
	Highcharts.chart('timelineGraph', {
		chart: {
			type: 'xrange',
			zoomType: 'x'
		},
		credits: {
			enabled: false
		},
		title: {
			text: 'Bangalore Metro'
		},
		xAxis: {
			type: 'datetime',
			min: new Date('2018/01/01').getTime(),
			max: new Date('2018/09/30').getTime(),
			plotLines: $scope.data['lines']
		},
		yAxis: {
			title: {
				text: ''
			},
			categories: ['Engine', 'Brake', 'Signal System'],
			reversed: true
		},
		series: [{
			name: 'Train No - XX01',
			// pointPadding: 0,
			// groupPadding: 0,
			borderColor: 'gray',
			pointWidth: 20,
			data: $scope.data['timeLine'],
			dataLabels: {
				enabled: true
			}
		}]

	});
	}
});
