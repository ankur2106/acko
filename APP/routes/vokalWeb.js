var express = require('express');
var router = express.Router();
var session = require('express-session');

/* GET home page. */
router.get('/', function(req, res, next) {

	if(req.session){
		if(req.session.loginStatus){
			res.render('vokalWeb/index', { sessionToken:req.session.token });
		}
		else{
			res.render('vokalWeb/index', { sessionToken:req.session.token });
		}
	}
	else{
		res.redirect('/login');
	}
});

module.exports = router;
