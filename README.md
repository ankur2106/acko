# Insurance Of Things

## API:
* Django Rest API

## UI:
* NodeJS
* AngularJS
    
## Models:
* Python-Scipy
    
## Implementation Flow:
* Data Re-sampling
* Failure Detection Using Ensemble Techniques
* Stream Results To Front End